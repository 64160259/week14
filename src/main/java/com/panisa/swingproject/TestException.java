/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.panisa.swingproject;

/**
 *
 * @author ADMIN
 */
public class TestException {
    public static void main(String[] args) {
       try {
           throw new Exception("Hello Exception");
           
       } catch(ArrayIndexOutOfBoundsException e) {
           System.out.println("Array Exception");
       } catch(Exception e){
           System.out.println(e.getMessage());
       } finally {
           System.out.println("Final Statement");
       }
    }
}
